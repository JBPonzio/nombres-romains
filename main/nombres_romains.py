class NombresRomains:

    def conversion(self, nombre_arabe):

        result = ""

        if nombre_arabe == 1:
            result = 'I'
        elif nombre_arabe == 2:
            result = 'II'
        elif nombre_arabe == 3:
            result = 'III'
        elif nombre_arabe == 4:
            result = 'IV'
        elif nombre_arabe == 5:
            result = 'V'
        elif nombre_arabe == 6:
            result = 'VI'
        elif nombre_arabe == 7:
            result = 'VII'
        elif nombre_arabe == 8:
            result = 'VIII'
        elif nombre_arabe == 9:
            result = 'IX'
        elif nombre_arabe == 10:
            result = 'X'
        elif nombre_arabe == 11:
            result = 'XI'
        elif nombre_arabe == 12:
            result = 'XII'
        elif nombre_arabe == 13:
            result = 'XIII'
        elif nombre_arabe == 14:
            result = 'XIV'
        elif nombre_arabe == 15:
            result = 'XV'
        elif nombre_arabe == 16:
            result = 'XVI'
        elif nombre_arabe == 17:
            result = 'XVII'
        elif nombre_arabe == 18:
            result = 'XVIII'
        elif nombre_arabe == 19:
            result = 'XIX'
        elif nombre_arabe == 20:
            result = 'XX'
        elif nombre_arabe == 21:
            result = 'XXI'
        elif nombre_arabe == 22:
            result = 'XXII'
        elif nombre_arabe == 23:
            result = 'XXIII'
        elif nombre_arabe == 24:
            result = 'XXIV'
        elif nombre_arabe == 25:
            result = 'XXV'
        elif nombre_arabe == 26:
            result = 'XXVI'
        elif nombre_arabe == 27:
            result = 'XXVII'
        elif nombre_arabe == 28:
            result = 'XXVIII'
        elif nombre_arabe == 29:
            result = 'XXIX'
        elif nombre_arabe == 30:
            result = 'XXX'
        elif nombre_arabe == 31:
            result = 'XXXI'
        elif nombre_arabe == 32:
            result = 'XXXII'

        return result
