import unittest

from main.nombres_romains import NombresRomains


class TestNombresRomain(unittest.TestCase):
    nb_romains = NombresRomains()

    # NOTE - Transformation du chiffre arabe 1 en chiffre romain I
    def test_1_to_I(self):
        nombre_arabe = 1
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'I')

    def test_2_to_II(self):
        nombre_arabe = 2
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'II')

    def test_3_to_III(self):
        nombre_arabe = 3
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'III')

    def test_4_to_IV(self):
        nombre_arabe = 4
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'IV')

    def test_5_to_V(self):
        nombre_arabe = 5
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'V')

    def test_6_to_VI(self):
        nombre_arabe = 6
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'VI')

    def test_7_to_VII(self):
        nombre_arabe = 7
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'VII')

    def test_8_to_VIII(self):
        nombre_arabe = 8
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'VIII')

    def test_9_to_IX(self):
        nombre_arabe = 9
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'IX')

    def test_10_to_X(self):
        nombre_arabe = 10
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'X')

    def test_11_to_XI(self):
        nombre_arabe = 11
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XI')

    def test_12_to_XII(self):
        nombre_arabe = 12
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XII')

    def test_13_to_XIII(self):
        nombre_arabe = 13
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XIII')

    def test_14_to_XIV(self):
        nombre_arabe = 14
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XIV')

    def test_15_to_XV(self):
        nombre_arabe = 15
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XV')

    def test_16_to_XVI(self):
        nombre_arabe = 16
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XVI')

    def test_17_to_XVII(self):
        nombre_arabe = 17
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XVII')

    def test_18_to_XVIII(self):
        nombre_arabe = 18
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XVIII')

    def test_19_to_XIX(self):
        nombre_arabe = 19
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XIX')

    def test_20_to_XX(self):
        nombre_arabe = 20
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XX')

    def test_21_to_XX(self):
        nombre_arabe = 21
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXI')

    def test_22_to_XX(self):
        nombre_arabe = 22
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXII')

    def test_23_to_XX(self):
        nombre_arabe = 23
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXIII')

    def test_24_to_XX(self):
        nombre_arabe = 24
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXIV')

    def test_25_to_XXV(self):
        nombre_arabe = 25
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXV')

    def test_26_to_XXVI(self):
        nombre_arabe = 26
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXVI')

    def test_27_to_XXVII(self):
        nombre_arabe = 27
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXVII')

    def test_28_to_XXVIII(self):
        nombre_arabe = 28
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXVIII')

    def test_29_to_XXIX(self):
        nombre_arabe = 29
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXIX')

    def test_30_to_XXX(self):
        nombre_arabe = 30
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXX')

    def test_31_to_XXXI(self):
        nombre_arabe = 31
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXXI')

    def test_32_to_XXXII(self):
        nombre_arabe = 32
        self.assertEqual(self.nb_romains.conversion(nombre_arabe), 'XXXII')


if __name__ == '__main__':
    unittest.main()
